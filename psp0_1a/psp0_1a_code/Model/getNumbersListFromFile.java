import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class getNumbersListFromFile {
    public LinkedList data;
    private static String FILENAME;

    public getNumbersListFromFile(String FILENAME) {
        this.FILENAME=FILENAME;
        this.data=new LinkedList();
        readFromFile();
    }
    public LinkedList getList(){
        return data;
    }
    private void readFromFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                this.data.InsertLast(Integer.parseInt(sCurrentLine));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }        
    }

    public void writeToFile(double stdv){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {

            String content = "Thes standart dev is: "+stdv+"\n";

            bw.write(content);
            bw.newLine();

            // no need to close it.
            //bw.close();

            System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

}