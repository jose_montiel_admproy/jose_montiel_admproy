import java.util.*;
import java.lang.Math;
public class getStv {
    public static double calculateStv(LinkedList data) throws ErrorInGetstv{
        double mean;
        try{
            mean= getMean(data);
            System.out.println("the mean is: "+mean);
        }
        catch(ErrorInGetstv e){
            throw new ErrorInGetstv("Internal error. Index out of bounds");
        }
        int size= data.length();
        double sum=0;
        for(int i=0;i<size;i++){
            try{
                double tmp=mean-(double)data.at(i);
                sum= sum + (tmp*tmp);
            }
            catch(IndexOutOfBounds e){
                throw new ErrorInGetstv("Internal error. Index out of bounds");
            }
        }
        double sigma2= sum/(size-1);
        return Math.sqrt(sigma2);

    }
    public static double getMean(LinkedList data) throws ErrorInGetstv{
        int size= data.length();
        System.out.println("the size is: "+size);

        double sum=0;
        for(int i=0;i<size;i++){
            try{
                System.out.println("the sum is: "+sum);
                sum= sum+data.at(i);
            }
            catch(IndexOutOfBounds e){
                throw new ErrorInGetstv("Internal error. Index out of bounds");
            }
        }
        return sum/(size);
    }

}