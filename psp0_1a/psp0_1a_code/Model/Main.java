import java.util.*;

public class Main {
    public static void main(String[]args){
        String inputFileName="in.txt";
        String outputFileName="out.txt";
        LinkedList data;

        getNumbersListFromFile reader= new getNumbersListFromFile(inputFileName);
        data=reader.getList();
        double stdv=-1;
        try{
            stdv= getStv.calculateStv(data);
        }catch(ErrorInGetstv e){
            System.out.println(e);
        }
        System.out.println(stdv);
        reader= new getNumbersListFromFile(outputFileName);
        reader.writeToFile(stdv);

    }
}