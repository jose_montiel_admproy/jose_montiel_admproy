class IndexOutOfBounds extends Exception {
   public IndexOutOfBounds(String msg){
      super(msg);
   }
}

class ErrorInGetstv extends Exception {
   public ErrorInGetstv(String msg){
      super(msg);
   }
}