import java.util.*;
public class LinkedList {
    private Node head;
    public LinkedList() {
        this.head=null;
    }
    public void InsertBeg(String data) {
        Node aux= this.head;
        this.head= new Node(data,aux);
    }

    public void InsertLast(String data) {
        Node aux = this.head;
        if(aux==null){
            this.head=new Node(data);
        }
        else{
            while(aux.getNext()!=null){
                aux= aux.getNext();
            }
            aux.setNext(new Node(data));
        }
    }
    public void DeleteLast() {
        Node aux = this.head;
        if(aux==null){

        }else if(aux.getNext()==null){
            this.head=null;
        }else{
            while(aux.getNext().getNext()!=null){
                aux= aux.getNext();
            }
            aux.setNext(null);
        }
    }
    public String setAt(String data, int index) throws IndexOutOfBounds{
        int size= length();
        if(index<size){
            Node aux = this.head;
            for(int i=0;i<index;i++){
                aux= aux.getNext();
            }
            String oldData=aux.getData();
            aux.setData(data);
            return oldData;
        }else{
            throw new IndexOutOfBounds("The index mention is not set.");
        }
    }

    public void pushAt(String data, int index) throws IndexOutOfBounds{
        int size= length();
        if(index<size){
            Node aux = this.head;
            for(int i=0;i<index-1;i++){
                aux= aux.getNext();
            }
            Node tmp= aux.getNext();
            aux.setNext(new Node(data, tmp));
        }else{
            throw new IndexOutOfBounds("The index required is not set.");
        }
    }

    public int length(){
        Node aux = this.head;
        int i=1;
        if(aux==null){
            return 0;
        }else{
            while(aux.getNext()!=null){
                aux= aux.getNext();
                i++;
            }
            return i;
        }

    }

    public String at(int index) throws IndexOutOfBounds{
        int size= length();
        if(index<size){
            Node aux = this.head;
            for(int i=0;i<index;i++){
                aux= aux.getNext();
            }
            return aux.getData();
        }else{
            throw new IndexOutOfBounds("The index required is not set.");
        }
    }
}